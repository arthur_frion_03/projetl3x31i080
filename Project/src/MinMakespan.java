import metier.LPT;
import metier.LSA;
import metier.RMA;

import java.util.ArrayList;
import java.util.Collections;

public class MinMakespan {
    public static void minMakespanNotIr(ArrayList<Integer> d, int n) {
        LSA lsa = new LSA(d, n);
        LPT lpt = new LPT(d, n);
        RMA rma = new RMA(d, n);
        int maxLSA = lsa.getMax();
        int maxLPT = lpt.getMax();
        int maxRMA = rma.getMax();
        int borneMax = borneMax(d);
        int borneMoy = borneMoy(d, n);
        System.out.println("Borne inférieur \"maximum\" = " + borneMax);
        System.out.println("Borne inférieur \"moyenne\" = " + borneMoy + "\n");
        System.out.println("Résultat LSA = " + maxLSA);
        System.out.println("Ratio LSA = " + ratio(maxLSA, borneMax, borneMoy) + "\n");
        System.out.println("Résultat LPT = " + maxLPT);
        System.out.println("Ratio LPT = " + ratio(maxLPT, borneMax, borneMoy) + "\n");
        System.out.println("Résultat RMA = " + maxRMA);
        System.out.println("Ratio RMA = " + ratio(maxRMA, borneMax, borneMoy) + "\n");
    }

    public static void minMakespanIr(ArrayList<ArrayList<Integer>> dk, int k, int n) {
        float ratioTotalLSA = 0;
        float ratioTotalLPT = 0;
        float ratioTotalRMA = 0;
        for (ArrayList<Integer> d : dk) {
            LSA lsa = new LSA(d, n);
            LPT lpt = new LPT(d, n);
            RMA rma = new RMA(d, n);
            int maxLSA = lsa.getMax();
            int maxLPT = lpt.getMax();
            int maxRMA = rma.getMax();
            int borneMax = borneMax(d);
            int borneMoy = borneMoy(d, n);
            ratioTotalLSA += ratio(maxLSA, borneMax, borneMoy);
            ratioTotalLPT += ratio(maxLPT, borneMax, borneMoy);
            ratioTotalRMA += ratio(maxRMA, borneMax, borneMoy);
            System.out.println("ratio moyen LSA = " + ratioTotalLSA / k);
            System.out.println("ratio moyen LPT = " + ratioTotalLPT / k);
            System.out.println("ratio moyen RMA = " + ratioTotalRMA / k + "\n");
        }
    }

    private static int borneMax(ArrayList<Integer> D) {
        Collections.sort(D);
        return D.get(D.size() - 1);
    }

    private static int borneMoy(ArrayList<Integer> D, int n) {
        int tpsT = 0;
        for (int val : D) tpsT += val;
        return (int) Math.ceil(tpsT / (float) n);
    }

    private static float ratio(int resu, int borneMax, int borneMoy) {
        int max = Math.max(borneMax, borneMoy);
        return resu / (float) max;
    }
}
