import metier.GenerateTask;

import java.util.ArrayList;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        menu();
    }

    private static void menu() {
        int enter = 0;
        Scanner saisie = new Scanner(System.in);
        while (enter < 1 || enter > 3) {
            System.out.println("\t[MENU]\n");
            System.out.println("[1] : Système I");
            System.out.println("[2] : Système I'");
            System.out.println("[3] : Système choisit");
            System.out.println("Sélectionnez la fonction à utiliser en notant sur numéro assigné :");
            if (saisie.hasNextInt()) {
                enter = saisie.nextInt();
            } else {
                Object trashValue = saisie.next();
                System.out.println("La valeur entrée n'est pas un int.\n\n");
            }
        }
        int m = 0;
        int n = 0;
        int k = 0;
        int dmin = 0;
        int dmax = 0;

        // Système I
        if (enter == 1) {
            System.out.println("Système I Sélectionné ");
            while (m < 1) {
                System.out.print("Entrez le nombre de machine. \nm = ");
                if (saisie.hasNextInt()) {
                    m = saisie.nextInt();
                } else {
                    Object trashValue = saisie.next();
                    System.out.println("La valeur entrée n'est pas un int.\n\n");
                }
            }
            ArrayList<Integer> d = GenerateTask.generateV1(m);
            MinMakespan.minMakespanNotIr(d, m);
        }

        // Système I'
        if (enter == 2) {
            System.out.println("Système I' Sélectionné ");
            while (m < 1) {
                System.out.print("Entrez le nombre de machine. \nm = ");
                if (saisie.hasNextInt()) {
                    m = saisie.nextInt();
                } else {
                    Object trashValue = saisie.next();
                    System.out.println("La valeur entrée n'est pas un int.\n\n");
                }
            }
            ArrayList<Integer> d = GenerateTask.generateV2(m);
            MinMakespan.minMakespanNotIr(d, m);
        }

        // Système choisit
        if (enter == 3) {
            System.out.println("Système Random Sélectionné ");
            while (m < 1) {
                System.out.print("Entrez le nombre de machine. \nm = ");
                if (saisie.hasNextInt()) {
                    m = saisie.nextInt();
                } else {
                    Object trashValue = saisie.next();
                    System.out.println("La valeur entrée n'est pas un int.\n\n");
                }
            }

            while (n < 1) {
                System.out.print("\nEntrez le nombre de tâches. \nn = ");
                if (saisie.hasNextInt()) {
                    n = saisie.nextInt();
                } else {
                    Object trashValue = saisie.next();
                    System.out.println("La valeur entrée n'est pas un int.\n\n");
                }
            }

            while (k < 1) {
                System.out.print("\nEntrez le nombre d'instance. \nk = ");
                if (saisie.hasNextInt()) {
                    k = saisie.nextInt();
                } else {
                    Object trashValue = saisie.next();
                    System.out.println("La valeur entrée n'est pas un int.\n\n");
                }
            }

            while (dmin < 1) {
                System.out.print("\nEntrez la durée minimum des tâches. \ndmin = ");
                if (saisie.hasNextInt()) {
                    dmin = saisie.nextInt();
                } else {
                    Object trashValue = saisie.next();
                    System.out.println("La valeur entrée n'est pas un int.\n\n");
                }
            }

            while (dmax < 1 || dmax <= dmin) {
                System.out.print("\nEntrez la durée maximum des tâches. \ndmax = ");
                if (saisie.hasNextInt()) {
                    dmax = saisie.nextInt();
                } else {
                    Object trashValue = saisie.next();
                    System.out.println("La valeur entrée n'est pas bonne.\n\n");
                }
            }
            ArrayList<ArrayList<Integer>> dk = new ArrayList<>();
            for (int instance = 0; instance < k; instance++) {
                ArrayList<Integer> d = GenerateTask.generateV3(n, dmin, dmax);
                dk.add(d);
            }
            MinMakespan.minMakespanIr(dk, k, m);
        }
    }
}
