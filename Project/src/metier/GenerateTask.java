package metier;

import java.util.ArrayList;

public class GenerateTask {
    public static ArrayList<Integer> generateV1(int m) {
        ArrayList<Integer> tasks = new ArrayList<>();
        for (int i = 0; i < Math.pow(m, 2); i++) {
            tasks.add(1);
        }
        tasks.add(m);
        return tasks;
    }

    public static ArrayList<Integer> generateV2(int m) {
        int nombreTaches = 2 * m + 1;
        ArrayList<Integer> tasks = new ArrayList<>();
        int i;
        int e = 1;
        //On ajoute Les 3 tâches de durée n.
        tasks.add(m);
        tasks.add(m);
        tasks.add(m);
        //On ajoute 2 tâche n+1
        for (i = 1; i < nombreTaches - 3; i = i + 2) {
            tasks.add(m + e);
            tasks.add(m + e);
            e++;
        }
        if (i > nombreTaches) {
            tasks.remove(tasks.size() - 1);
        }
        return tasks;
    }

    public static ArrayList<Integer> generateV3(int n, int dmin, int dmax) {
        ArrayList<Integer> tasks = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            tasks.add((int) (Math.random() * (dmax - dmin)));
        }
        return tasks;
    }
}
