package metier;

import java.util.Collections;
import java.util.List;

public class LPT extends SchedulingAlgorithm {

    public LPT(List<Integer> d, int n) {
        super(d, n);
    }

    @Override
    protected void sort() {
        D.sort(Collections.reverseOrder());
        for (int val : D) {
            Collections.sort(M);
            M.set(0, M.get(0) + val);
        }
    }
}
