package metier;

import java.util.Collections;
import java.util.List;

public class LSA extends SchedulingAlgorithm {

    public LSA(List<Integer> d, int n) {
        super(d, n);
    }

    @Override
    protected void sort() {
        for (int val : D) {
            Collections.sort(M);
            M.set(0, M.get(0) + val);
        }
    }
}
