package metier;

import java.util.List;

public class RMA extends SchedulingAlgorithm {
    public RMA(List<Integer> d, int n) {
        super(d, n);
    }

    @Override
    protected void sort() {
        for (int val : D) {
            int machine = (int) (Math.random() * (M.size()));
            M.set(machine, M.get(machine) + val);
        }
    }
}
