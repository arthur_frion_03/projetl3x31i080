package metier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class SchedulingAlgorithm {
    protected List<Integer> D;
    protected List<Integer> M;

    protected SchedulingAlgorithm(List<Integer> d, int n) {
        D = d;
        ArrayList<Integer> m = new ArrayList<>();
        for (int i = 0; i < n; i++) m.add(0);
        M = m;
    }

    protected abstract void sort();

    public int getMax() {
        this.sort();
        Collections.sort(M);
        return M.get(M.size() - 1);
    }

    @Override
    public String toString() {
        return "SchedulingAlgorithm{" + "D=" + D + ", M=" + M + '}';
    }
}
